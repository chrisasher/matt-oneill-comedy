<!-- Start the loop for Gigs CPT and call the fields to get the ACF fields-->
<?php
// Start the loop.
if(have_posts()){
	while ( have_posts() ){
		the_post();

		$image = get_the_post_thumbnail_url();
		$title = get_the_title();
		$id = get_the_ID();
		$bodyclass = get_body_class();

		?>
		<?php
		// End of the loop.
	}
}
?>


<?php 

// args
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'gigs'
);

// query
$the_query = new WP_Query( $args );

?>
<section class="gigs-main tex-center bg-siteBg overflow-hidden bg-siteBg relative">
<?php
/* Template Name: Gigs */
get_header();
?>
    <div class="container relative">
        <div class="other-badge">
            <img class="block mx-auto hidden lg:block" src="<?php echo get_template_directory_uri(); ?>/img/matt_badge.png" alt="">
        </div>
    </div>
    <div class="container relative mt-5">
        <div class="diary-element text-center lg:pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/3 block mx-auto">
            <p class="bg-bgColor px-4 py-1" href="#">Dates for the diary</p>
        </div>
        <!-- Query and ACF stuff -->
        <?php if( $the_query->have_posts() ): ?>
        <div class="justify-center items-center relative p-1">
            <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="flex gig-details flex-wrap border-bottom">
                    <div class="w-full lg:w-1/3 px-2 text-center lg:text-right">
                        <?php $announcement = get_field('date'); ?>
                        <?php if($announcement) : ?>
                            <p><?php the_field('date'); ?></p>
                        <?php endif ; ?>
                    </div>
                    <div class="w-full lg:w-1/3 px-2 text-textColor uppercase text-center lg:text-center">
                        <?php $location = get_field('location'); ?>
                        <?php if($location): ?>
                            <p><?php the_field('location'); ?></p>
                        <?php endif; ?>
                    </div>
                    <div class="w-full lg:w-1/3 px-2 text-center lg:text-left">
                        <?php $time = get_field('time'); ?>
                        <?php if($time): ?>
                            <p><?php the_field('time'); ?></p>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    <!-- End WP query -->
    </div>                 
</section>

<?php wp_reset_query(); ?>


<?php
get_footer(); ?>