// * Created by chris on 12/04/2016.
// */
jQuery(document).ready(function($){

    $('#matt-svg').addClass('animate-svg');


    if($('body').hasClass('home')) {
        $('.navigation-holder').hide();    
    }
    $('.menu__icon').click(function(){
        $('body').toggleClass('menu_shown');

        var navLinks = $('ul#menu-main-menu li');
        navLinks.each(function (link, index) {
            // console.log(index);
            
            if(index.hasClass('animated')) {
                index.removeClass('animated');
            } 
            index.addClass('animated');
        })
    });

    $('.home-badge').hide();

    var rect = d3.select('#matt-svg');
    // this selects a rectangle.  Let's say that it starts at 
    // the origin, or even off screen in your case   

        // this changes the x and y attributes
        // of the rectangle to 10 and 20 respectively
        // using a transition over a 250ms duration.

    $(document).keyup(function(event){        
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            setTimeout(() => {
                $('.badge').fadeIn('slow');
            }, 3000);
            $('.socials').fadeOut('slow');
            setTimeout(() => {
                $('.navigation-holder').fadeIn('slow');
            }, 2000);
            $('.enter-button').fadeOut();
            // $('#matt-svg').fadeOut('4000');
            $('.logo-resize').slideUp('slow');
            $('.matt-main-page').addClass('transform-full');
            $('.logo-resize').css('position', 'unset'); 
        }
    });

    $(document).on('resize', function(){
        if(!$('body').hasClass('home')) {
            if($(window).width() > 991) {
                $('.navigation-holder').css('position', 'unset');
                $('.navigation-holder').css('margin-left', 'unset');
            } else {
                $('.navigation-holder').css('position', 'fixed');
                $('.navigation-holder').css('margin-left', 'unset');
            }
        }
    })

    $('.enter-button').on('click', function(){
        setTimeout(() => {
            $('.badge').fadeIn('slow');
        }, 3000);
        $('.socials').fadeOut('slow');
        setTimeout(() => {
            $('.navigation-holder').fadeIn('slow');
        }, 2000);
        $('.enter-button').fadeOut();
        // $('#matt-svg').fadeOut('4000');
        $('.logo-resize').slideUp('slow');
        $('.matt-main-page').addClass('transform-full');
        $('.logo-resize').css('position', 'unset'); 
    });
})

jQuery(window).load(function($){

})

jQuery(window).resize(function($){

})

function sizetorow(divheight, target, adjustment){
    var $ = jQuery;
    if($(window).width() > 800){
        var maxHeight = 0;

        $(target).css('min-height',maxHeight);
        var maxHeight = $(divheight).height() - adjustment;
        console.log(maxHeight);
        //$(target).each(function(){
        //
        //})
        setTimeout(function(){
            $(target).css('min-height',maxHeight);
        },600)
    }
}

function makesquare(target){
    var $ = jQuery;
    var width = $(target).width();
    $(target).height(width);
}