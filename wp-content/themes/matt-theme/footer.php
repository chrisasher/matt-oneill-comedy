<section id="footer">
    <div class="matt-image bg-siteBg pt-1 mt-3">
        <img class="block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/cropped-backdrop.jpg" alt="">
    </div>    
</section>
<?php wp_footer(); ?>
</body>
</html>
