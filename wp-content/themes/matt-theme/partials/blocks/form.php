<?php
/**
 * Block Name: Form Block
 */
?>
<div class="diary-element text-center pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/4 block mx-auto">
    <p class="bg-bgColor px-4 py-1" href="#"><?php the_field('page_heading', get_the_ID()); ?></p>
</div>
<div class="flex justify-center items-center">
    <div class="w-full lg:w-1/2 px-2">
        <?php the_field('contact_shortcode'); ?>
    </div>
</div>