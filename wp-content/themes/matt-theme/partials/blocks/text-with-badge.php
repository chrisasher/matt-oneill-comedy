<?php
/**
 * Block Name: Text & Badge Block
 */
?>
<div class="container relative">
    <div class="badge">
        <img class="block mx-auto hidden lg:block" src="<?php the_field('text_image') ?>" alt="">
    </div>
    <div class="diary-element text-center lg:pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/3 block mx-auto">
        <p class="bg-bgColor px-4 py-1"><?php the_field('text_image_decorated') ?></p>
    </div>
</div>