<?php
/**
 * Block Name: About Block
 */
?>
<div class="diary-element text-center pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/5 block mx-auto">
    <p class="bg-bgColor px-4 py-1" href="#"><?php the_field('page_heading', get_the_ID()); ?></p>
</div>
<!-- Query and ACF stuff -->
<div class="relative p-1 about-matt">
    <div class="flex justify-center items-center">
        <div class="w-4/5 lg:w-5/12 px-2">
            <p><?php the_field('matt_description'); ?></p>
        </div>
    </div>
</div>
<div class="gutenberg-content pt-2 pb-6 lg:pt-6">
    <div class="container flex flex-wrap lg:flex-row justify-around">
        <?php if( have_rows('testimonials') ): ?>
            <?php while( have_rows('testimonials') ): the_row(); ?> 
                <div class="w-full overflow-hidden sm:w-1/3 md:w-1/4 pb-3 lg:pb-0">
                    <p><?php the_sub_field('testimonial') ?></p>
                    <span><?php the_sub_field('author') ?></span>
                </div>
            <?php endwhile; ?>   
        <?php endif; ?>
    </div>
</div>
<!-- End WP query -->