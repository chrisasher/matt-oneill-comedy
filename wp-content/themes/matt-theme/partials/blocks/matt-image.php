<?php
/**
 * Block Name: Image Block
 */
?>
<div class="matt-image bg-siteBg absolute pt-1">
    <img class="block mx-auto" src="<?php the_field('the_image'); ?>" alt="">
</div>  