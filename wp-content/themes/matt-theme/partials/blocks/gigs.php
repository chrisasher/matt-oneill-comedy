<?php
/**
 * Block Name: Gigs Block
 */
?>
<?php 
// args
$args = array(
	'numberposts'	=> -1,
	'post_type'		=> 'gigs'
);
// query
$the_query = new WP_Query( $args );

?>
    <div class="diary-element text-center lg:pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/3 block mx-auto">
        <p class="bg-bgColor px-4 py-1" href="#"><?php the_field('page_heading', get_the_ID()); ?></p>
    </div>
    <!-- Query and ACF stuff -->
    <?php if( $the_query->have_posts() ): ?>
        <div class="justify-center items-center relative p-1">
            <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="flex gig-details flex-wrap border-bottom">
                    <div class="w-full lg:w-1/3 px-2 text-center lg:text-right">
                        <p><?php the_field('date', get_the_ID()); ?></p>
                    </div>
                    <div class="w-full lg:w-1/3 px-2 text-textColor uppercase text-center lg:text-center">
                        <p><?php the_field('location', get_the_ID()); ?></p>
                    </div>
                    <div class="w-full lg:w-1/3 px-2 text-center lg:text-left">
                        <p><?php the_field('time', get_the_ID()); ?></p>
                    </div>
                </div>
    <?php endwhile; ?>
    </div>
<?php endif; ?>
<!-- End WP query -->