
<?php
/**
 * Block Name: Badge Block
 */
?>
<div class="container relative">
    <div class="other-badge">
        <img class="block mx-auto hidden lg:block" src="<?php echo get_template_directory_uri(); ?>/img/matt_badge.png" alt="">
    </div>
</div>