<?php
/**
 * Block Name: Free eyebrow text Block
 */
?>
<div class="relative p-1 about-matt">
    <div class="flex justify-center items-center">
        <div class="w-4/5 lg:w-1/3 px-2">
            <!-- <p>Matt O’Neill is a Stand-Up Comedian from the UK, but based in Melbourne, Australia. </p> -->
            <?php the_field('free_text'); ?>
        </div>
    </div>
</div>