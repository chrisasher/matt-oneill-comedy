<?php
// Blocks

function oink_theme_setup() {
   
    // Add Align Wide and Full Width options to blocks.
    // This is done by adding the corresponding classname to the block’s wrapper.
    add_theme_support( 'align-wide' );

    // Stop users picking their own colours in the block editor
    add_theme_support( 'disable-custom-colors' );

    // Stop users from changing font sizes
    add_theme_support('disable-custom-font-sizes');

    // Custom Editor Colours
    add_theme_support( 'editor-color-palette', array(
        // array(
        //     'name' => __( 'Colour 1', 'oink' ),
        //     'slug' => 'bg-colour-1',
        //     'color' => '#26193b',
        // ),
        // array(
        //     'name' => __( 'Colour 2', 'oink' ),
        //     'slug' => 'bg-colour-2',
        //     'color' => '#f4b014',
        // ),
        array(
            'name' => __( 'Black', 'oink' ),
            'slug' => 'bg-black',
            'color' => '#000000',
        ),
        array(
            'name' => __( 'White', 'oink' ),
            'slug' => 'bg-white',
            'color' => '#ffffff',
        ),
        array(
            'name' => __( 'Grey', 'oink' ),
            'slug' => 'bg-grey',
            'color' => '#f2f2f2',
        ),

    ) );

    // Add a stylesheet for editing in the Dashboard
    add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
    add_editor_style( 'dashboard.css' );
}

add_action( 'after_setup_theme', 'oink_theme_setup' );


 

//Remove Gutenberg Styling from the frontend
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );

// Add Reusable blocks to menu
add_action( 'admin_menu', 'linked_url' );
function linked_url() {
add_menu_page( 'linked_url', 'Reusable Blocks', 'read', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}

add_action( 'admin_menu' , 'linkedurl_function' );
function linkedurl_function() {
global $menu;
$menu[1][2] = "/wp-admin/edit.php?post_type=wp_block";
}

// Register ACF Blocks
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'gigs',
        'title'             => __('Gigs block'),
        'description'       => __('Gigs block.'),
        'render_template'   => 'partials/blocks/gigs.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'gigs', 'events' ),
    ));

    // acf_register_block_type(array(
    //     'name'              => 'badge',
    //     'title'             => __('Badge Image'),
    //     'description'       => __('A custom badge block.'),
    //     'render_template'   => 'partials/blocks/badge.php',
    //     'category'          => 'formatting',
    //     'icon'              => 'dashicons-layout',
    //     'keywords'          => array( 'Hero', 'hero' ),
    // ));

    acf_register_block_type(array(
        'name'              => 'about',
        'title'             => __('About block'),
        'description'       => __('About block.'),
        'render_template'   => 'partials/blocks/about.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-layout',
        'keywords'          => array( 'about', 'events' ),
    ));

    acf_register_block_type(array(
        'name'              => 'form',
        'title'             => __('Contact Form'),
        'description'       => __('A custom form block.'),
        'render_template'   => 'partials/blocks/form.php',
        'category'          => 'formatting',
        'icon'              => 'dashicons-format-status',
        'keywords'          => array( 'info', 'details' ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}