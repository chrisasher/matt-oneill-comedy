<?php
/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function theme_scripts() {
//	wp_enqueue_script( 'masonry-js',
//		get_stylesheet_directory_uri().'/plugs/masonry.js',
//		array( 'jquery' )
//	);
//	wp_enqueue_script( 'fancybox-js',
//		get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.js',
//		array( 'jquery' )
//	);

//	wp_enqueue_script( 'theme-nav', get_template_directory_uri() . '/plugs/navbar.js', array('jquery'));

//	wp_enqueue_style('fancybox-css',get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.css');
//	wp_enqueue_style('woo-small-css',get_home_url().'/wp2016/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css', array(),'','all and (max-width:800px)');
	
	wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.8/js/all.js');
	
	$version = '1';

    wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), $version, false);

    wp_enqueue_script( 'tweenmax', 'https://cdnjs.cloudflare.com/ajax/libs/d3/5.16.0/d3.min.js', true);

    wp_enqueue_style( 'poppins', 'https://fonts.googleapis.com/css?family=Poppins:400,600,700&display=swap' );

    wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css' );
    
    wp_enqueue_style( 'style', get_template_directory_uri() . '/dist/css/style.css?v='.$version );

}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );