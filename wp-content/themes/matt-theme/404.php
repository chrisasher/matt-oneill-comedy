<?php get_header(); ?>
<div class="container text-center pt-4">
    <h1>404</h1>
    <h4>Sorry, the page you have requested is not available</h4>
</div>
<?php get_footer(); ?>
