<section class="about-main tex-center bg-siteBg overflow-hidden bg-siteBg relative">
<?php
/* Template Name: About */
get_header(); 
?>
    <div class="container relative">
        <div class="other-badge">
            <img class="block mx-auto hidden lg:block" src="<?php echo get_template_directory_uri(); ?>/img/matt_badge.png" alt="">
        </div>
    </div>
    <div class="container mt-5">
        <div class="diary-element text-center pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/5 block mx-auto">
            <p class="bg-bgColor px-4 py-1" href="#">About</p>
        </div>
        <!-- Query and ACF stuff -->
		<div class="relative p-1 about-matt">
            <div class="flex justify-center items-center">
                <div class="w-4/5 lg:w-2/5 px-2">
                    <p>Matt O’Neill is a Stand-Up Comedian from the UK, but based in Melbourne, Australia. </p>
                </div>
            </div>
        </div>
		<div class="gutenberg-content pt-2 pb-6 lg:pt-4">
			<div class="container flex flex-row justify-center">
			<?php
			// TO SHOW THE PAGE CONTENTS
			while ( have_posts() ) : the_post(); ?>
				<div class="width flex">
					<?php the_content(); ?> <!-- Page Content -->
				</div>
			<?php
			endwhile; 
			wp_reset_query();
			?>
			</div>
		</div>
    <!-- End WP query -->
    </div>
</section>

<?php
get_footer(); ?>