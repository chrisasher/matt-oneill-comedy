<?php
get_header();

$getBagePreference = get_field('badge_preference');
?>
<?php if($getBagePreference): ?>
<div class="container relative">
	<div class="other-badge">
		<img class="block mx-auto hidden xl:block" src="<?php echo get_template_directory_uri(); ?>/img/matt_badge.png" alt="">
	</div>
</div>
<?php endif; ?>
<div class="container mt-5">
<?php
// Start the loop.
if(have_posts()){
	while ( have_posts() ){
		the_post();

		$image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg';
		$title = get_the_title();
		$id = get_the_ID();
		$bodyclass = get_body_class();

		?>
		<?php the_content() ?>
		<?php
		// End of the loop.
	}
}
?>
</div>

<?php
get_footer();