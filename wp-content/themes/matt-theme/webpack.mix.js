const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.options({ imgLoaderOptions: { enabled: false } })
    .sourceMaps(false, 'source-map')
    // .version()
    .js('main.js', 'js')
    .sass('sass/style.scss', 'css')
    .setPublicPath('./dist')
    .options({
        processCssUrls: true,
        postCss: [ tailwindcss('./tailwind.config.js') ],
    })
    .browserSync({
        proxy: 'localhost/dev/matt-comedy',
        files: [
         'sass/**/*',
         '*.php',
         '*.js',
        ]
    })