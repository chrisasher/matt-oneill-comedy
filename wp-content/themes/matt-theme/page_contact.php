<section class="contact-main tex-center bg-siteBg lg:overflow-hidden bg-siteBg relative">
<?php
/* Template Name: Contact */
get_header();
?>
    <div class="container relative">
        <div class="other-badge">
            <img class="block mx-auto hidden xl:block" src="<?php echo get_template_directory_uri(); ?>/img/matt_badge.png" alt="">
        </div>
    </div>
    <div class="container mt-5">
        <div class="diary-element text-center pt-3 pb-2 lg:py-4 text-white uppercase w-3/5 lg:w-1/4 block mx-auto">
            <p class="bg-bgColor px-4 py-1" href="#">Contact</p>
        </div>
        <!-- Query and ACF stuff -->
        <div class="relative p-1">
                <div class="flex justify-center items-center">
                    <div class="w-full lg:w-1/2 px-2">
                        <?php echo do_shortcode('[contact-form-7 id="30" title="Contact form 1"]'); ?>
                    </div>
                </div>
        </div>
    <!-- End WP query -->
    </div>
</section>

<?php
get_footer(); ?>