<section class="matt-main relative">
<?php
get_header();
?>
	<div class="container relative">
		<div class="badge">
			<img class="badge-style hidden lg:block" src="<?php echo get_template_directory_uri(); ?>/img/badge.png" alt="">
		</div>
	</div>
	<div class="image-holder">
		<img class="block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/Normal-backdrop.jpg" alt="">
	</div>
</section>



<section class="overlay text-white relative hidden">
	<div class="nav-holder flex justify-center py-3">
		<?php
			if(get_theme_mod("facebook_link")){
				echo '<a href="'.get_theme_mod("facebook_link").'" class="social-link">Facebook</a>';
			}
			if(get_theme_mod("twitter_link")){
				echo '<a href="'.get_theme_mod("twitter_link").'" class="social-link">Twitter</a>';
			}
			if(get_theme_mod("instagram_link")){
				echo '<a href="'.get_theme_mod("instagram_link").'" class="social-link">Instagram</a>';
			}
			if(get_theme_mod("youtube_link")){
				echo '<a href="'.get_theme_mod("youtube_link").'" class="social-link">Youtube</a>';
			}
		?>
	</div>
	<!-- Third layer in z-index -->
	<div class="matt-clip relative">
		<!-- <img class="block mx-auto fix-this" src="<?//php echo get_template_directory_uri(); ?>/img/Normal-backdrop.jpg" alt=""> -->
		<div class="clip-image relative">
			<img class="block mx-auto absolute" src="<?php echo get_template_directory_uri(); ?>/img/backdrop-lp-bk.svg" alt="">
		</div>
		<!-- <div class="enter">
			<img class="enter-button block mx-auto w-9" src="<?php echo get_template_directory_uri(); ?>/img/enter-button.svg" alt="">
		</div> -->
	</div>	
</section>

<section class="over">
	<div class="nav-holder flex justify-center py-3">
		<?php
			if(get_theme_mod("facebook_link")){
				echo '<a href="'.get_theme_mod("facebook_link").'" class="social-link">Facebook</a>';
			}
			if(get_theme_mod("twitter_link")){
				echo '<a href="'.get_theme_mod("twitter_link").'" class="social-link">Twitter</a>';
			}
			if(get_theme_mod("instagram_link")){
				echo '<a href="'.get_theme_mod("instagram_link").'" class="social-link">Instagram</a>';
			}
			if(get_theme_mod("youtube_link")){
				echo '<a href="'.get_theme_mod("youtube_link").'" class="social-link">Youtube</a>';
			}
		?>
	</div>
	<!-- Third layer in z-index -->
	<div class="matt-clip relative">
		<!-- <img class="block mx-auto fix-this" src="<?//php echo get_template_directory_uri(); ?>/img/Normal-backdrop.jpg" alt=""> -->
		<div class="clip-image relative">
			<img class="block mx-auto absolute" src="<?php echo get_template_directory_uri(); ?>/img/backdrop-lp-bk.svg" alt="">
		</div>
		<!-- <div class="enter">
			<img class="enter-button block mx-auto w-9" src="<?php echo get_template_directory_uri(); ?>/img/enter-button.svg" alt="">
		</div> -->
	</div>	
</section>