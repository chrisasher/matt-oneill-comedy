<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<nav>
	<div class="mobile-menu flex justify-between flex lg:hidden">
		<div class="logo-holder">
			<a href=" <?php echo get_home_url(); ?>"><img class="block mx-auto" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt=""></a>
		</div>

		<a class="menu__icon mobile"><span></span></a>
	</div>
	<div class="navigation-holder flex justify-center items-center pb-3 uppercase text-white">
		<?php
			if(get_theme_mod("facebook_link")){
				echo '<a target="_blank" href="'.get_theme_mod("facebook_link").'" class="social-link">Facebook</a>';
			}
			if(get_theme_mod("twitter_link")){
				echo '<a target="_blank" href="'.get_theme_mod("twitter_link").'" class="social-link">Twitter</a>';
			}
			if(get_theme_mod("instagram_link")){
				echo '<a target="_blank" href="'.get_theme_mod("instagram_link").'" class="social-link">Instagram</a>';
			}
			if(get_theme_mod("youtube_link")){
				echo '<a target="_blank" href="'.get_theme_mod("youtube_link").'" class="social-link">Youtube</a>';
			}
		?>
		<div class="logo-holder lg:block lg:px-4">
			<a href=" <?php echo get_home_url(); ?>"><img class="block mx-auto hidden lg:flex" src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt=""></a>
		</div>
		<?php
			wp_nav_menu(array('theme_location' => 'primary'));
		?>
	</div>
</nav>