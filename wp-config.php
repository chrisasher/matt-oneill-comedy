<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'oink' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ' 5tp<t=C!Rk|B13)E:*.U2WQ9@-`L*uwEAnAq@!2CL]nXjz#[^Qx[)DAXuGm5F_?' );
define( 'SECURE_AUTH_KEY',  'hyvT)4Oc53PocKD[IoC./5A`di:~-{0Oh0)msZ)iP]qYejzkM/Mf*u~u#hRkx85K' );
define( 'LOGGED_IN_KEY',    'Ded?^cOmjE#_r|,,;UQ|w;)h LmDs,i[P|}PZZm!079dNvB]}/WlQ7%*lzG}jvL=' );
define( 'NONCE_KEY',        '/o3DHe g0KHgO3Z/?1%}avA?!td]@{sks>t-s&iF?)ofw6PD7z+JP@?2i$CDG&EL' );
define( 'AUTH_SALT',        'M4;zlqe89*.:ueP. ,mOdXj|_{F],6>#NtJJS$ !}t5hp#f6@$)01J6/nHUNI~@`' );
define( 'SECURE_AUTH_SALT', '.J/]3A<JaI<WdlhgQh|od}_$/991^ChBVY5YG A0zj.v6>D,?`]`;wj>KKf%J[QX' );
define( 'LOGGED_IN_SALT',   '+~T/M0g-$F[0EsAhoN4ig19ns(HtsJ[iE9MkDHKA???MP)9{*d:n/l,xTC<{02k|' );
define( 'NONCE_SALT',       '2[*|C{1)!.j{:LH_n([i9z~YBmwDco>suJ~LBF!-7kex WCtBN)k|Z_m&YGn&y7P' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
